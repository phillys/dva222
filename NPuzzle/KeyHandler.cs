﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPuzzle.Contracts;

namespace NPuzzle
{
    class KeyHandler : IInputHandler
    {
        public BoardDirection? Handle()
        {
            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.UpArrow:
                    return BoardDirection.UP;
                case ConsoleKey.DownArrow:
                    return BoardDirection.DOWN;
                case ConsoleKey.LeftArrow:
                    return BoardDirection.LEFT;
                case ConsoleKey.RightArrow:
                    return BoardDirection.RIGHT;                    
            }

            return null;
        }
    }
}
