﻿using System;
using System.Text;

namespace NPuzzle
{
    class Board
    {
        private int Dimension;
        private Tile[,] Tiles;
        private EmptyTile EmptyTile = new EmptyTile();
        private Point EmptyTilePosition;

        public Board(int dimension = 4)
        {
            Dimension = dimension;

            Tiles = new Tile[Dimension, Dimension];
            int tileValue = 1;

            for (int y = 0; y < Dimension; y++)
            {
                for (int x = 0; x < Dimension; x++)
                {
                    Tiles[y, x] = new NumberTile(tileValue);
                    tileValue++;
                }
            }

            EmptyTilePosition = new Point(Dimension - 1, Dimension - 1);
            Tiles[EmptyTilePosition.X, EmptyTilePosition.Y] = EmptyTile;

            Shuffle();
        }

        public void Shuffle()
        {
            var random = new Random();

            // Naive shuffling that scales exponentially with a max limit of 10 000 random moves
            for(var i = 0; i < Math.Min(Math.Pow(10, Dimension), Math.Pow(10, 4)); i++) 
            {
                MoveEmptyTile((BoardDirection) random.Next(4));
            }
        }

        public void Render()
        {
            var maxStrLen = ((Dimension * Dimension) - 1).ToString().Length;
            var rowSeparator = new StringBuilder()
                                    .Insert(0, new StringBuilder("+").Insert(1, "-", maxStrLen).ToString(), Dimension)
                                    .Append('+');

            Console.WriteLine(rowSeparator);

            for (int y = 0; y < Dimension; y++)
            {                
                for (int x = 0; x < Dimension; x++)
                {
                    GetTile(x, y).Render(maxStrLen);                  
                }
                Console.WriteLine("|");
                Console.WriteLine(rowSeparator);
            }
        }

        private Tile GetTile(int x, int y)
        {
            return Tiles[y, x];
        }

        private Tile GetTile(Point p)
        {
            return Tiles[p.Y, p.X];
        }

        public bool IsOrdered()
        {
            int? prevTileValue = 0;
            int maxNumber = Dimension * Dimension - 1;

            foreach (Tile tile in Tiles)
            {
                if (tile.Value == null && prevTileValue != maxNumber || (tile.Value - 1 > prevTileValue))
                {
                    return false;
                }
                prevTileValue = tile.Value;
            }

             return true;
        }

        private void SwapEmptyTile(Point newPosition)
        {
            Tiles[EmptyTilePosition.Y, EmptyTilePosition.X] = GetTile(newPosition);
            Tiles[newPosition.Y, newPosition.X] = EmptyTile;
            EmptyTilePosition = newPosition;
        }

        public int MoveEmptyTile(BoardDirection direction)
        {
            switch (direction)
            {
                case BoardDirection.UP:
                    return MoveEmptyTileUp();
                case BoardDirection.DOWN:
                    return MoveEmptyTileDown();
                case BoardDirection.LEFT:
                    return MoveEmptyTileLeft();
                case BoardDirection.RIGHT:
                    return MoveEmptyTileRight();
            }

            return 0;
        }

        private int MoveEmptyTileUp()
        { 
            if(EmptyTilePosition.Y > 0)
            {
                SwapEmptyTile(new Point(EmptyTilePosition.X, EmptyTilePosition.Y - 1));
                return 1;
            }
            return 0;
        }

        private int MoveEmptyTileDown()
        {
            if (EmptyTilePosition.Y < Dimension - 1)
            {
                SwapEmptyTile(new Point(EmptyTilePosition.X, EmptyTilePosition.Y + 1));
                return 1;
            }
            return 0;
        }

        private int MoveEmptyTileLeft()
        {
            if (EmptyTilePosition.X > 0)
            {
                SwapEmptyTile(new Point(EmptyTilePosition.X - 1, EmptyTilePosition.Y));
                return 1;
            }
            return 0;
        }

        private int MoveEmptyTileRight()
        {
            if (EmptyTilePosition.X < Dimension - 1)
            {
                SwapEmptyTile(new Point(EmptyTilePosition.X + 1, EmptyTilePosition.Y));
                return 1;
            }
            return 0;
        }
    }
}