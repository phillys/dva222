﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPuzzle
{
    class Program
    {
        static void Main(string[] args)
        {
            var puzzle = new Puzzle(new KeyHandler());
            puzzle.Run();
        }
    }
}
