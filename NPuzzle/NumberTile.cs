﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPuzzle
{
    class NumberTile : Tile
    {
        public NumberTile(int value)
        {
            Value = value;
        }

        public override void Render(int width)
        {
            Console.Write($"|{Value.ToString().PadLeft(width)}");
        }
    }
}
