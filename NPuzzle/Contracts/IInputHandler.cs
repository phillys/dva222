﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPuzzle.Contracts
{
    // Can be used to implement other input methods
    interface IInputHandler
    {
        BoardDirection? Handle();
    }
}
