﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPuzzle.Contracts;

namespace NPuzzle
{
    class Puzzle
    {
        private Board Board;
        private IInputHandler InputHandler;
        private int MovesCount;

        public Puzzle(IInputHandler inputHandler)
        {
            Board = new Board(3);
            InputHandler = inputHandler;
        }

        public bool IsCompleted()
        {
            return Board.IsOrdered();
        }

        private void WriteMovesCount()
        {
            Console.WriteLine($"Moves: {MovesCount}{Environment.NewLine}");
        }

        public void Run()
        {
            Console.CursorVisible = false;
            Console.BackgroundColor = ConsoleColor.Black;

            while (true)
            {
                Console.Clear();

                WriteMovesCount();

                Board.Render();

                if (IsCompleted())
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Puzzle solved!");
                    Console.ReadKey();
                    break;
                }

                var direction = InputHandler.Handle();

                if(direction != null)
                {
                    MovesCount += Board.MoveEmptyTile((BoardDirection) direction);
                }
            }
        }
    }
}
