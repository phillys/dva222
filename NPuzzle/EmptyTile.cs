﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPuzzle
{
    class EmptyTile : Tile
    {
        public override void Render(int width)
        {
            Console.Write("|");
            Console.BackgroundColor = ConsoleColor.Green;
            Console.Write("".PadLeft(width));
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }
}
