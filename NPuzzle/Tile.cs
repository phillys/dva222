﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPuzzle
{
    abstract class Tile
    {
        public int? Value { get; protected set; }
        abstract public void Render(int width);
    }
}
