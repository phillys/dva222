﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPuzzle
{
    public enum BoardDirection
    {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }
}
