﻿using System;
using System.Drawing;
using Assignment2.Interfaces;

namespace Assignment2
{
    abstract class Rectangle : ICollidable
    {
        protected Pen pen = new Pen(Color.White);
        public PointF Position { get; }
        public Size Size { get; }

        protected Rectangle(int x, int y, int width, int height)
            : this(new PointF(x, y), new Size(width, height)) {}

        protected Rectangle(PointF position, Size size)
        {
            Position = position;
            Size = size;
        }

        public bool CheckForCollision(Ball ball)
        {
            // https://stackoverflow.com/questions/401847/circle-rectangle-collision-detection-intersection/402010#402010
            int halfWidth = Size.Width / 2;
            int halfHeight = Size.Height / 2;
            var circleDistance = new PointF(
                Math.Abs(ball.Position.X - (Position.X + halfWidth)),
                Math.Abs(ball.Position.Y - (Position.Y + halfHeight))
            );

            if (circleDistance.X > halfWidth + ball.Radius) { return false; }
            if (circleDistance.Y > halfHeight + ball.Radius) { return false; }

            if (circleDistance.X <= halfWidth) { return true; }
            if (circleDistance.Y <= halfHeight) { return true; }

            double cornerDistanceSq = Math.Pow(circleDistance.X - halfWidth, 2) +
                                      Math.Pow(circleDistance.Y - halfHeight, 2);

            return cornerDistanceSq <= Math.Pow(ball.Radius, 2);
        }

        public void Draw(Graphics g)
        {
            g.DrawRectangle(pen, Position.X, Position.Y, Size.Width, Size.Height);
        }

        public abstract void OnCollision(Ball ball);
    }
}
