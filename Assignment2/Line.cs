﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Assignment2.Interfaces;

namespace Assignment2
{
    abstract class Line : ICollidable
    {
        protected Pen pen = new Pen(Color.White);
        public PointF Position1 { get; }
        public PointF Position2 { get; }

        protected Line(int x1, int y1, int x2, int y2)
            : this(new PointF(x1, y1), new PointF(x2, y2)) {}

        protected Line(PointF position1, PointF position2)
        {
            Position1 = position1;
            Position2 = position2;
        }

        public bool CheckForCollision(Ball ball)
        {
            // http://csharphelper.com/blog/2014/09/determine-where-a-line-intersects-a-circle-in-c/
            var d = new PointF(Position2.X - Position1.X, Position2.Y - Position1.Y);
            var f = new PointF(Position1.X - ball.Position.X, Position1.Y - ball.Position.Y);
            var f2 = new PointF(Position2.X - ball.Position.X, Position2.Y - ball.Position.Y);
            double a = Math.Pow(d.X, 2) + Math.Pow(d.Y, 2);
            double b = 2 * (f.X * d.X + f.Y * d.Y);
            double c = Math.Pow(f.X, 2) + Math.Pow(f.Y, 2) - Math.Pow(ball.Radius, 2);
            double discriminant = Math.Pow(b, 2) - 4 * a * c;

            if (discriminant < 0) return false;

            double discriminantRoot = Math.Sqrt(discriminant);

            double t1 = (-b - discriminantRoot) / (2 * a);
            double t2 = (-b + discriminantRoot) / (2 * a);

            if (t1 >= 0 && t1 <= 1 || t2 >= 0 && t2 <= 1)
            {
                return true;
            }

            return false;
        }

        public void Draw(Graphics g)
        {
            g.DrawLine(pen, Position1, Position2);
        }

        public abstract void OnCollision(Ball ball);
    }
}
