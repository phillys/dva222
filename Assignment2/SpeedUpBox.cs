﻿using System;
using System.Drawing;

namespace Assignment2
{
    class SpeedUpBox : Rectangle
    {
        public SpeedUpBox(int x, int y, int width, int height) : base(x, y, width, height)
        {
            pen = new Pen(Color.Red);
        }

        public override void OnCollision(Ball ball)
        {
            ball.IncreaseSpeed();
        }
    }
}
