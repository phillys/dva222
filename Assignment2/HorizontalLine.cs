﻿using System;
using System.Drawing;

namespace Assignment2
{
    class HorizontalLine : Line
    {
        public HorizontalLine(int x1, int y1, int x2) : base(x1, y1, x2, y1)
        {
            pen = new Pen(Color.Green);
        }

        public override void OnCollision(Ball ball)
        {

            if (ball.Speed.Y > 0)
            {
                ball.Position = new PointF(ball.Position.X, Position1.Y - ball.Radius - 1);
            }
            else if (ball.Speed.Y < 0)
            {
                ball.Position = new PointF(ball.Position.X, Position1.Y + ball.Radius + 1);
            }
            else
            {
                ball.InvertDirection(Ball.Axis.X);
            }

            ball.InvertDirection(Ball.Axis.Y);
        }
    }
}
