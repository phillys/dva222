﻿using System;
using System.Drawing;

namespace Assignment2
{
    class VerticalLine : Line
    {
        public VerticalLine(int x1, int y1, int y2) : base(x1, y1, x1, y2)
        {
            pen = new Pen(Color.Yellow);
        }

        public override void OnCollision(Ball ball)
        {
            if (ball.Speed.X > 0)
            {
                ball.Position = new PointF(Position1.X - ball.Radius - 1, ball.Position.Y);
            }
            else if (ball.Speed.X < 0)
            {
                ball.Position = new PointF(Position1.X + ball.Radius + 1, ball.Position.Y);
            }
            else
            {
                ball.InvertDirection(Ball.Axis.Y);
            }

            ball.InvertDirection(Ball.Axis.X);
        }
    }
}
