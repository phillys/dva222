﻿using System;
using System.Drawing;
using Assignment2.Interfaces;

namespace Assignment2
{
    public class Ball : IDrawable
    {
        private Pen pen = new Pen(Color.White);
        private PointF position;
        private int radius;
        private Vector speed;
        public enum Axis { X, Y };

        private int Diameter { get => radius * 2; }

        public PointF Position
        {
            get => position;
            set { position = value; }
        }
        
        public int Radius { get => radius; }
        
        public Vector Speed
        {
            get => speed;
            set
            {
                value.X = Math.Max(Math.Min(value.X, Diameter), -Diameter);
                value.Y = Math.Max(Math.Min(value.Y, Diameter), -Diameter);
                speed = value;
            }
        }
 
        public Ball(PointF position, int radius)
		{
            this.position = position;
			this.radius = radius;
		}

		public Ball(int x, int y, int radius)
            : this(new PointF(x, y), radius) {}

        public void Draw(Graphics g)
        {
            g.DrawEllipse(pen, position.X - Radius, position.Y - Radius, Diameter, Diameter);
        }

        public void DecreaseSpeed()
        {
            if (Math.Abs(Speed.X) * 0.95f >= 1 && Math.Abs(Speed.Y) * 0.95f >= 1)
            {
                Speed = new Vector(Speed.X * 0.95f, Speed.Y * 0.95f);
            }
        }

        public void IncreaseSpeed()
        {
            if (Math.Abs(Speed.X) * 1.05f <= Diameter && Math.Abs(Speed.Y) * 1.05f <= Diameter)
            {
                Speed = new Vector(speed.X * 1.05f, speed.Y * 1.05f);
            }
        }

        public void InvertDirection(Axis axis)
        {
            switch (axis)
            {
                case Axis.X:
                    speed.X *= -1;
                    break;
                case Axis.Y:
                    speed.Y *= -1;
                    break;
            }
        }

        public void Move()
		{
			position.X = position.X + speed.X;
			position.Y = position.Y + speed.Y;
		}       
    }
}
