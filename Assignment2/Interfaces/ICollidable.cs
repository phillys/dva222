﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2.Interfaces
{
    interface ICollidable : IDrawable
    {
        bool CheckForCollision(Ball ball);
        void OnCollision(Ball ball);
    }
}
