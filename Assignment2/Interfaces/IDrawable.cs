﻿using System.Drawing;

namespace Assignment2.Interfaces
{
	public interface IDrawable
	{
        void Draw(Graphics g);
	}
}
