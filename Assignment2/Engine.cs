﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Assignment2.Interfaces;

namespace Assignment2
{
	public class Engine
	{
        private const int MaxBalls = 20;
        private MainForm form;
		private Timer timer;

		private ISet<Ball> balls = new HashSet<Ball>();
		private ISet<ICollidable> collidables = new HashSet<ICollidable>();

        private Random random = new Random();

		public Engine()
		{
			form = new MainForm();
			timer = new Timer();

			AddBall();
            collidables.Add(new SpeedUpBox(170, 150, 70, 70));
            collidables.Add(new SpeedUpBox(650, 320, 50, 110));
            collidables.Add(new SpeedDownBox(520, 70, 70, 70));
            collidables.Add(new SpeedDownBox(80, 480, 320, 45));
            collidables.Add(new VerticalLine(740, 40, 540));
            collidables.Add(new VerticalLine(20, 80, form.Height));
            collidables.Add(new VerticalLine(100, 70, 240));
            collidables.Add(new HorizontalLine(80, 40, 320));
            collidables.Add(new HorizontalLine(400, 40, 680));
            collidables.Add(new HorizontalLine(130, 120, 360));
            collidables.Add(new HorizontalLine(200, 430, 600));
            collidables.Add(new HorizontalLine(510, 490, 720));
            collidables.Add(new HorizontalLine(15, 580, 700));
        }

        public void Run()
		{
			form.Paint += new PaintEventHandler(Draw);

			timer.Tick += new EventHandler(TimerEventHandler);
			timer.Interval = 1000/25;
			timer.Start();

			Application.Run(form);
		}

		private void AddBall()
		{
            var ball = new Ball(form.Width / 2, form.Height / 2, 10);

            do { ball.Speed = new Vector(random.Next(-5, 5), random.Next(-5, 5)); }
            while (ball.Speed.X == 0 || ball.Speed.Y == 0);

            balls.Add(ball);

            //var ball1 = new Ball(40, 10, 10);
            //ball1.Speed = new Vector(1, 1);
            //balls.Add(ball1);

            //var ball2 = new Ball(250, 90, 10);
            //ball2.Speed = new Vector(-2, -20);
            //balls.Add(ball2);

            //var ball3 = new Ball(250, 90, 10);
            //ball3.Speed = new Vector(0, -1);
            //balls.Add(ball3);

            //var ball4 = new Ball(20, 40, 10);
            //ball4.Speed = new Vector(1, 0);
            //balls.Add(ball4);
        }

        private void Collide(Ball ball, ICollidable collidable)
        {
            if (collidable.CheckForCollision(ball))
            {
                collidable.OnCollision(ball);
            }
        }

        private bool IsBallInView(Ball ball)
        {
            return ball.Position.X > -ball.Radius &&
                   ball.Position.Y > -ball.Radius &&
                   ball.Position.X < form.Width + ball.Radius &&
                   ball.Position.Y < form.Height + ball.Radius;
        }

		private void TimerEventHandler(Object obj, EventArgs args)
		{
            if (random.Next(100) < 25 && balls.Count < MaxBalls) AddBall();

            foreach (var ball in new HashSet<Ball>(balls))
			{
                ball.Move();

                foreach (var collidable in collidables)
                {
                    Collide(ball, collidable);
                }

                if (! IsBallInView(ball))
                {
                    balls.Remove(ball);
                }
            }

			form.Refresh();
		}

		private void Draw(Object obj, PaintEventArgs args)
		{
            foreach (var collidable in collidables)
            {
                collidable.Draw(args.Graphics);
            }
            foreach (var ball in balls)
			{
				ball.Draw(args.Graphics);
			}
		}
	}
}
