﻿using System;
using System.Drawing;

namespace Assignment2
{
    class SpeedDownBox : Rectangle
    {
        public SpeedDownBox(int x, int y, int width, int height) : base(x, y, width, height)
        {
            pen = new Pen(Color.Blue);
        }

        public override void OnCollision(Ball ball)
        {
            ball.DecreaseSpeed();
        }
    }
}
