﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Assignment2
{
	public class MainForm : Form
	{
		public MainForm() : base()
		{
            Text = "DVA222 - Assignment 2";
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.DoubleBuffer, true);
            Width = 800;
			Height = 600;
            BackColor = Color.Black;
            FormBorderStyle = FormBorderStyle.None; // Titlebar is included in width and height
            StartPosition = FormStartPosition.Manual;
            Location = new Point(0, 0);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
