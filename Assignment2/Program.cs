﻿using System.Windows.Forms;
using System;

namespace Assignment2
{
	class MainClass
	{
		public static void Main(string[] args)
		{
            Engine engine = new Engine();
            engine.Run();
        }
	}
}
