﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace Assignment3
{
    class CityConsoleLookup
    {
        private IDictionary<GeoLocation, City> cities = new HashDictionary<GeoLocation, City>(4000, new GeoLocationEqualityComparer());
        private CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
        private bool run = true;
        private string filename;

        public CityConsoleLookup()
            : this("cities100000.txt") { }

        public CityConsoleLookup(string filename)
        {
            this.filename = filename;
        }

        public void Run()
        {
            LoadCitiesFromFile(filename);

            while (run)
            {
                var latitude = AskForDouble("Latitude: ");
                var longitude = AskForDouble("Longitude: ");

                if (cities.TryGetValue(new GeoLocation(latitude, longitude), out City city))
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(city);
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("No city matches given coordinates!");
                }

                Console.ResetColor();

                AskRunAgain();
            };
        }

        private void LoadCitiesFromFile(string filename)
        {
            using (var file = new StreamReader(filename))
            {
                string line;

                while ((line = file.ReadLine()) != null)
                {
                    var city = line.Split('\t');
                    cities.Add(
                        new GeoLocation(double.Parse(city[1], culture), double.Parse(city[2], culture)),
                        new City(city[0], int.Parse(city[3]))
                    );
                }
            }

            Console.WriteLine($"Number of cities: {cities.Count}");
        }

        private Double AskForDouble(string text)
        {
            double fpNumber;
            string fpString;

            do
            {
                Console.Write(text);
                fpString = Console.ReadLine();
            } while (!double.TryParse(fpString, NumberStyles.Float, culture, out fpNumber));

            return fpNumber;
        }

        private void AskRunAgain()
        {
            ConsoleKey runAgain;

            do
            {
                Console.Write("Run again? (y/n) ");
                runAgain = Console.ReadKey().Key;
                Console.Clear();
            } while (runAgain != ConsoleKey.Y && runAgain != ConsoleKey.N);

            run = (runAgain == ConsoleKey.Y);
        }
    }
}
