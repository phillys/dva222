﻿using System;

namespace Assignment3
{
    class City
    {
        public string Name { get; private set; }
        public int Population { get; private set; }

        public City(string name, int population)
        {
            Name = name;
            Population = population;
        }

        public override string ToString()
        {
            return $"Name: {Name}{Environment.NewLine}Population: {Population}";
        }
    }
}
