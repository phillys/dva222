﻿using System;

namespace Assignment3
{
    class GeoLocation : IEquatable<GeoLocation>
    {
        private const double tolerance = 0.00001;
        public double Latitude { get; private set; }
        public double Longitude { get; private set; }
        
        public GeoLocation(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        // Static since I reuse this implementation in GeoLocationEqualityComparer
        public static bool Equals(GeoLocation x, GeoLocation y)
        {
            return Math.Abs(x.Latitude - y.Latitude) <= tolerance &&
                   Math.Abs(x.Longitude - y.Longitude) <= tolerance;
        }

        //public static bool operator ==(GeoLocation x, GeoLocation y)
        //{
        //    return Equals(x, y);
        //}

        //public static bool operator !=(GeoLocation x, GeoLocation y)
        //{
        //    return ! Equals(x, y);
        //}

        //public override bool Equals(object obj)
        //{
        //    // Check for null values and compare run-time types.
        //    if (obj == null || GetType() != obj.GetType()) return false;

        //    GeoLocation other = (GeoLocation) obj;

        //    return Equals(other);
        //}

        public bool Equals(GeoLocation other)
        {
            return Equals(this, other);
        }

        // Static since I reuse this implementation in GeoLocationEqualityComparer
        public static int GetHashCode(GeoLocation geo)
        {
            return geo.Latitude.GetHashCode() ^ geo.Longitude.GetHashCode();
        }

        public override int GetHashCode()
        {
            return GetHashCode(this);
        }

        public override string ToString()
        {
            return $"{Latitude}, {Longitude}";
        }
    }
}
