﻿using System.Collections.Generic;

namespace Assignment3
{
    class GeoLocationEqualityComparer : IEqualityComparer<GeoLocation>
    {
        // Maybe the comparer should be responsible for the implementation?

        public bool Equals(GeoLocation x, GeoLocation y)
        {
            return GeoLocation.Equals(x, y);
        }

        public int GetHashCode(GeoLocation geo)
        {
            return GeoLocation.GetHashCode(geo);
        }
    }
}
