﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Assignment3
{
    class HashDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {
        private LinkedList<KeyValuePair<TKey, TValue>>[] buckets;
        private int count;
        private IEqualityComparer<TKey> comparer;
        private PrimeGenerator primeGenerator = new PrimeGenerator();
        private const float loadFactorThreshold = .75f;
      
        public HashDictionary() : this(1, null) { }

        public HashDictionary(int capacity) : this(capacity, null) { }

        public HashDictionary(IEqualityComparer<TKey> keyComparer) : this(1, keyComparer) { }

        public HashDictionary(int capacity, IEqualityComparer<TKey> keyComparer)
        {
            if (capacity < 0) throw new ArgumentOutOfRangeException();

            buckets = new LinkedList<KeyValuePair<TKey, TValue>>[CalculcateSize(capacity)];
            comparer = keyComparer ?? EqualityComparer<TKey>.Default;
        }

        private float loadFactor => (float) count / (float) buckets.Length;

        private int CalculcateSize(int capacity)
        {
            return primeGenerator.GetNextPrime(Convert.ToInt32(Math.Round(capacity * 1.3f)));
        }

        private LinkedList<KeyValuePair<TKey, TValue>> GetBucket(TKey key)
        {
            return buckets[GetIndex(key)];
        }

        private int GetIndex(TKey key, ref LinkedList<KeyValuePair<TKey, TValue>>[] buckets)
        {
            if (key == null) throw new ArgumentNullException("Key cannot be null.");
            return Math.Abs(comparer.GetHashCode(key) % buckets.Length);
        }

        private int GetIndex(TKey key)
        {
            return GetIndex(key, ref buckets);
        }

        private void Insert(TKey key, TValue value, bool add, ref LinkedList<KeyValuePair<TKey, TValue>>[] buckets, ref int count)
        {
            var i = GetIndex(key, ref buckets);

            if (buckets[i] == null)
            {
                buckets[i] = new LinkedList<KeyValuePair<TKey, TValue>>();
            }
            else
            {
                foreach (var entry in buckets[i])
                {
                    if (comparer.Equals(entry.Key, key))
                    {
                        if (add) throw new ArgumentException("An item with the same key has already been added.");
                        buckets[i].Remove(entry); // Works since we don't iterate further
                        count--;
                        break;
                    }
                }
            }

            buckets[i].AddLast(new KeyValuePair<TKey, TValue>(key, value));

            count++;
        }

        private void Insert(TKey key, TValue value, bool add)
        {
            if (loadFactor > loadFactorThreshold) Resize();

            Insert(key, value, add, ref buckets, ref count);
        }

        private void Resize()
        {
            Console.WriteLine($"[Resize]: { Count } entries in { this.buckets.Length } buckets (load factor { loadFactor })");

            var buckets = new LinkedList<KeyValuePair<TKey, TValue>>[CalculcateSize(this.buckets.Length)];
            var count = 0;

            foreach (var entry in this)
            {
                Insert(entry.Key, entry.Value, true, ref buckets, ref count);
            }

            this.buckets = buckets;
            this.count = count;
        }

        private bool TryGetEntryInBucket(TKey key, LinkedList<KeyValuePair<TKey, TValue>> bucket, out KeyValuePair<TKey, TValue> entry)
        {
            if (bucket != null)
            {
                foreach (var e in bucket)
                {
                    if (comparer.Equals(e.Key, key))
                    {
                        entry = e;
                        return true;
                    }
                }
            }

            entry = default(KeyValuePair<TKey, TValue>);
            return false;
        }

        public TValue this[TKey key]
        {
            get
            {
                var found = TryGetEntryInBucket(key, GetBucket(key), out KeyValuePair<TKey, TValue> entry);
                if (! found) throw new KeyNotFoundException();
                return entry.Value;
            }
            set
            {
                Insert(key, value, false);
            }
        }

        public ICollection<TKey> Keys 
        {
            get
            {
                var keys = new List<TKey>();

                foreach (var bucket in buckets)
                {
                    if (bucket == null) continue;

                    foreach (var entry in bucket)
                    {
                        keys.Add(entry.Key);
                    }
                }

                return keys;
            }
        }

        public ICollection<TValue> Values
        {
            get
            {
                var values = new List<TValue>();

                foreach (var bucket in buckets)
                {
                    if (bucket == null) continue;

                    foreach (var entry in bucket)
                    {
                        values.Add(entry.Value);
                    }
                }

                return values;
            }
        }

        public int Count => count;

        public bool IsReadOnly => false;

        public void Add(TKey key, TValue value)
        {
            Insert(key, value, true);
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            Add(item.Key, item.Value);
        }

        public void Clear()
        {
            if (count > 0)
            {
                Array.Clear(buckets, 0, buckets.Length);
                count = 0;
            }
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            var bucket = GetBucket(item.Key);

            if (bucket == null) return false;

            foreach (var entry in bucket)
            {
                if (comparer.Equals(entry.Key, item.Key) && EqualityComparer<TValue>.Default.Equals(entry.Value, item.Value))
                {
                    return true;
                }
            }

            return false;
        }

        public bool ContainsKey(TKey key)
        {
            var bucket = GetBucket(key);

            if (bucket == null) return false;

            foreach (var entry in bucket)
            {
                if (comparer.Equals(entry.Key, key))
                {
                    return true;
                }
            }

            return false;
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int index)
        {
            if (array == null) throw new ArgumentNullException("Array cannot be null.");
            if (index < 0 || index > array.Length) throw new ArgumentOutOfRangeException("Index must be greater than 0 and less than array length");
            if (array.Length - index < count) throw new ArgumentException("Array must have enough space to store all elements from given index to end");
            
            foreach (var bucket in buckets)
            {
                if (bucket == null) continue;

                foreach (var entry in bucket)
                {
                    array[index++] = new KeyValuePair<TKey, TValue>(entry.Key, entry.Value);
                }
            }
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return new HashDirectoryEnumerator(this);
        }

        public bool Remove(TKey key)
        {
            var bucket = GetBucket(key);
            var found = TryGetEntryInBucket(key, bucket, out KeyValuePair<TKey, TValue> entry);

            if (found)
            {
                bucket.Remove(entry);
                count--;
            }

            return found;
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            var bucket = GetBucket(item.Key);

            if (bucket == null) return false;

            foreach (var entry in bucket)
            {
                if (comparer.Equals(entry.Key, item.Key) && EqualityComparer<TValue>.Default.Equals(entry.Value, item.Value))
                {
                    bucket.Remove(entry); // Works since we don't iterate further
                    count--;
                    return true;
                }
            }

            return false;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            var bucket = GetBucket(key);
            var found = TryGetEntryInBucket(key, bucket, out KeyValuePair<TKey, TValue> entry);
            value = entry.Value;
            return found;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new HashDirectoryEnumerator(this);
        }

        class HashDirectoryEnumerator : IEnumerator<KeyValuePair<TKey, TValue>>
        {
            // https://msdn.microsoft.com/en-us/library/system.collections.ienumerator.current
            private int currentBucketIndex = -1;
            private LinkedListNode<KeyValuePair<TKey, TValue>> currentNode;
            private HashDictionary<TKey, TValue> dictionary;
            private LinkedList<KeyValuePair<TKey, TValue>> currentBucket => dictionary.buckets[currentBucketIndex];

            public KeyValuePair<TKey, TValue> Current => (currentNode == null) ? default(KeyValuePair<TKey, TValue>) : currentNode.Value;
            object IEnumerator.Current => this.Current;

            public HashDirectoryEnumerator(HashDictionary<TKey, TValue> dictionary)
            {
                this.dictionary = dictionary;
            }

            public void Dispose() {}

            private bool MoveToFirstNodeOfNextBucket()
            {
                currentBucketIndex++;

                while (currentBucketIndex < dictionary.buckets.Length)
                {
                    if (currentBucket == null)
                    {
                        currentBucketIndex++;
                    }
                    else
                    {
                        currentNode = currentBucket.First;
                        return true;
                    }
                }
                
                return false;
            }

            public bool MoveNext()
            {
                if (currentNode == null)
                {
                    return MoveToFirstNodeOfNextBucket();
                }

                var nextNode = currentNode.Next;

                if (nextNode == null)
                {
                    return MoveToFirstNodeOfNextBucket();
                }

                currentNode = nextNode;
                return true;
            }

            public void Reset()
            {
                currentBucketIndex = -1;
                currentNode = null;
            }
        }
    }
}
