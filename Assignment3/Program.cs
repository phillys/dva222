﻿using System;

namespace Assignment3
{
    class Program
    {
        static void Main(string[] args)
        {
            var cityConsoleLookup = new CityConsoleLookup();
            cityConsoleLookup.Run();
        }
    }
}
